// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.0;

import './Election.sol';

contract User {
    struct Info {
        uint userId;
        string name;
        address publicAddress;
    }

    Info public info;

    constructor(uint _userId, string memory _name, address _publicAddress) {
	    info = Info(_userId, _name, _publicAddress);
    }

    event electionCreated(address election);

    uint public electionId = 0;
    mapping (uint => address) public Elections;

    address[] public getElections;
    address[] public verifiedUsers;
    //BrightID
    bytes32 app = "snapshot";
    address authorizedSigners = 0xb1d71F62bEe34E9Fc349234C201090c33BCdF6DB;

    // function createElection (string[] memory _nda, uint[] memory _se) public returns(uint) {
	//     require(msg.sender == info.publicAddress, "Can't create election using other's contract");
    //     Election election = new Election(electionId, _nda, _se, msg.sender);
    //     Elections[electionId] = address(election);
    //     getElections.push(address(election));
    //     emit electionCreated(Elections[electionId]);
        
    //     electionId++;
    // }

    // function getInfo() public view returns (Info memory) {
    //     return info;
    // }
    function verify(
        address[] memory addrs,
        uint timestamp,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public {
        bytes32 message = keccak256(abi.encodePacked(app, addrs, timestamp));
        address signer = ecrecover(message, v, r, s);
        require(signer == authorizedSigners, "Not authorized");
        verifiedUsers.push(addrs[0]);
    }
    function getVerifiers() public view returns(address[] memory) {
        return verifiedUsers;
    } 
    // function retrieveElections() public view returns (address[] memory) {
    //     if(getElections == "undefined") {return [];}
    //     return getElections;
    // }
    // function update_name(string updated_name) public{
    //     require(msg.sender == info.publicAddress, "Only the owner can change!");
    //     info.name = updated_name;
    // }
}
