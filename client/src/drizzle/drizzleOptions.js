import MainContract from "../blockchainBuild/MainContract.json";

const drizzleOptions = {
    contracts: [MainContract]
}

export default drizzleOptions;