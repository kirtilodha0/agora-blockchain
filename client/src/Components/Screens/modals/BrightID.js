import { useState, useEffect } from "react";
import { Flex, Modal, Button, Card } from "rimble-ui";
import QRCode from "react-qr-code";

function BrightID({ MainContract, UserContract, account }) {
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState({
    addresses: [],
    timestamp: 0,
    v: 0,
    r: "",
    s: "",
  });
  const[verifiers,setVerifiers]=useState([]);
  const [content, setContent]=useState("Get BrightID verified!")
  const closeModal = (e) => {
    e.preventDefault();
    setIsOpen(false);
  };
let detail;

  const openModal = async (e) => {
    e.preventDefault();
    detail = await UserContract.getVerifiers()
      .call()
      .then((val) => {
        setVerifiers(val);
      });
      if(verifiers[0]=="0xa1C2668091b30CA136F065883bF8bE744bF6b37A"){
        setContent("You're verified");
      }
     else setIsOpen(true);
  };

  const value = `brightid://link-verification/http:%2f%2fnode.brightid.org/snapshot/${account}`;

  const brightid = () => {
    fetch(
      "https://app.brightid.org/node/v5/verifications/snapshot/0xa1c2668091b30ca136f065883bf8be744bf6b37a?signed=eth&timestamp=seconds",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setData({
          addresses: data.data.contextIds,
          timestamp: data.data.timestamp,
          v: data.data.sig.v,
          r: "0x" + data.data.sig.r,
          s: "0x" + data.data.sig.s,
        });
      })
      .catch((err) => {
        console.error(err);
      });
      
  };

  const getVerifiers = async (e) => {
    e.preventDefault();

    detail = await UserContract.getVerifiers()
      .call()
      .then((val) => {
        setVerifiers(val);
      });
    
  };
  
  console.log(verifiers);

  const provehumanity = async (e) => {
    const { addresses, timestamp, v, r, s } = data;
    e.preventDefault();
    try {
      window.toastProvider.addMessage("Processing your request.", {
        variant: "processing",
      });
      // console.log("add " + addresses + "time " + timestamp+"v ->"+v+ "r-> "+r+"s-> "+s);
      let response = await UserContract.verify(
        addresses,
        timestamp,
        v,
        r,
        s
      ).send({ from: account });

      window.toastProvider.addMessage("Success", {
        secondaryMessage: "Verification request sent. Thank you.",
        variant: "success",
      });
      console.log(response);

      setData({
        addresses: [],
        timestamp: 0,
        v: 0,
        r: "",
        s: "",
      });

    } catch (err) {
      console.log(err);
      window.toastProvider.addMessage("Failed", {
        secondaryMessage: "Please verify yourself through the steps.",
        variant: "failure",
      });
    }
  };

  useEffect(() => {
    brightid();
  }, []);

  return (
    <form>
      <div className="createElectionButton" onClick={openModal}>
        {content}
      </div>
      <Modal isOpen={isOpen}>
        <Card
          width={"90%"}
          height={"max-content"}
          style={{ maxWidth: "600px", padding: "20px" }}
          p={0}
        >
          <Button.Text
            style={{ margin: "0px" }}
            icononly
            icon={"Close"}
            color={"moon-gray"}
            position={"absolute"}
            top={0}
            right={0}
            mt={3}
            mr={3}
            onClick={closeModal}
          />

          <div style={{ margin: "10px", maxWidth: "400px", width: "90%" }}>
            <h4>Scan the QR Code to link to the BrightID!</h4>

            <br />
          </div>

          <QRCode title="snapshot" value={value} />
          <Button ml={3} type="submit" onClick={provehumanity}>
            Confirm Verification!
          </Button>
          <Button ml={3} type="submit" onClick={getVerifiers}>
            Verifiers
          </Button>
        </Card>
      </Modal>
    </form>
  );
}

export default BrightID;
