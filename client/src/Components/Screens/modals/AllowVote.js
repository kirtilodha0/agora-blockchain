import { useState } from 'react';
import { Flex, Modal, Button, Card } from "rimble-ui";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

function AllowVote({ Candidate, isActive, status, currentElectionDetails, CurrentElection, account }) {
    const [isOpen, setIsOpen] = useState(false);
    const [voters,setVoters]= useState([]);
	const [approvers,setApprovers]= useState([]);
	const [rejected,setRejected]= useState([]);
    const [address, setAddress] = useState("");

    const closeModal = e => {
        e.preventDefault();
        setIsOpen(false);
    };

    const openModal = e => {
        e.preventDefault();
        setIsOpen(true);
    };

    let data;
    
    const handleClick = async (e) => {
        e.preventDefault();
        setIsOpen(true);
        data = await CurrentElection.getRequesters().call()
        .then((val)=>{
            setVoters(val);
            
        })
        data = await CurrentElection.getApprovers().call()
        .then((val)=>{
            setApprovers(val);
            
        })
        data = await CurrentElection.getRejected().call()
        .then((val)=>{
            setRejected(val);
        })
        
    }
    const approveVoter = async(x)=>{

        try {
            window.toastProvider.addMessage("Processing your request.", {
                variant: "processing"
            })

            await CurrentElection.approveVoters(x).send({ from: account });

            window.toastProvider.addMessage("Approved", {
                secondaryMessage: "You have successfully requested! Thank you.",
                variant: "success"
            });

        } catch (err) {
            window.toastProvider.addMessage("Failed", {
                secondaryMessage: "Transaction failed. Try again",
                variant: "failure"
            });
        }

    }
    const rejectVoter = async(x)=>{
        
        try {
            window.toastProvider.addMessage("Processing your request.", {
                variant: "processing"
            })

            await CurrentElection.rejectVoters(x).send({ from: account });

            window.toastProvider.addMessage("Rejected", {
                secondaryMessage: "You have successfully requested! Thank you.",
                variant: "success"
            });

        } catch (err) {
            window.toastProvider.addMessage("Failed", {
                secondaryMessage: "Transaction failed. Try again",
                variant: "failure"
            });
        }

    }
    const sendInvite = async(x)=>{
        
        try {
            window.toastProvider.addMessage("Processing your request.", {
                variant: "processing"
            })

            await CurrentElection.invites(x).send({ from: account });

            window.toastProvider.addMessage("Sent", {
                secondaryMessage: "You have successfully requested! Thank you.",
                variant: "success"
            });

        } catch (err) {
            window.toastProvider.addMessage("Failed", {
                secondaryMessage: "Transaction failed. Try again",
                variant: "failure"
            });
        }

    }
    console.log(approvers);

    const checkVerification = (x)=>{
        for(let i=0;i<approvers.length;i++){
            if(x==approvers[i]) return true;
        }
        return false;
    }
    const isRejected = (x)=>{
        for(let i=0;i<rejected.length;i++){
            if(x==rejected[i]) return true;
        }
        return false;
    }
    
    const requestedVoters = voters.map((x)=>{
        return(
            <>
            <div>{x}
            {checkVerification(x) ? <b> - Approved </b> : isRejected(x) ? <b>- Rejected </b>:
            <>
            <button onClick={() => approveVoter(x)}>Approve</button>
            <button onClick={() => rejectVoter(x)}>Reject</button></>
        }
            </div>
            <br />
            </>
        )
    })
   
    return (
        <form>
            <div className="createElectionButton" onClick={handleClick}>
                Allow Voter
            </div>
            <Modal isOpen={isOpen}>
                <Card width={"100%"} height={"max-content"} style={{ maxWidth: "700px", padding:"25px" }} p={0}>
                    <Button.Text
                        style={{ margin: "0px" }}
                        icononly
                        icon={"Close"}
                        color={"moon-gray"}
                        position={"absolute"}
                        top={0}
                        right={0}
                        mt={3}
                        mr={3}
                        onClick={closeModal}
                    />

                    <div>
                        <h3>Approve for allowing people to vote</h3>
                        
      <label>Enter address to allow voting:
        <input 
          type="text" 
          value={address}
          onChange={(e) => setAddress(e.target.value)}
        />
      </label>
      
      <button onClick={() => sendInvite(address)}>Send Invite</button>
      <h3>Approved Voters</h3>
               {voters.length===0? "No requests": requestedVoters}
                        </div>
                </Card>
            </Modal>
        </form>
    );
}

export default AllowVote;