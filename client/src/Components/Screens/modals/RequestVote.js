import ReactStars from 'react-stars'
import React from 'react'
import { render } from 'react-dom'
import { useState } from 'react';
import { Flex, Modal, Button, Card, ToastMessage } from "rimble-ui";
import '../../styles/Modal.scss';
import { AVATARS, STATUS } from '../../constants';

function VoteModal({ Candidate, isActive, status, currentElectionDetails, CurrentElection, account }) {
    
    const handleVoteSubmit = async (e) => {
        e.preventDefault();
        
        try {
            window.toastProvider.addMessage("Processing your request.", {
                variant: "processing"
            })

            await CurrentElection.requestedVoters(account).send({ from: account });

            window.toastProvider.addMessage("Requested", {
                secondaryMessage: "You have successfully requested! Thank you.",
                variant: "success"
            });

        } catch (err) {
            window.toastProvider.addMessage("Failed", {
                secondaryMessage: "Transaction failed. Try again",
                variant: "failure"
            });
        }
        
    }

    return (
        <div>
            <div className="verifyButton" onClick={handleVoteSubmit}>
                       Request Vote
                    </div>
                    
        </div>
    );
}

export default VoteModal;