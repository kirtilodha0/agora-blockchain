import ReactStars from 'react-stars'
import React from 'react'
import { render } from 'react-dom'
import { useState } from 'react';
import { Flex, Modal, Button, Card, ToastMessage } from "rimble-ui";
import '../../styles/Modal.scss';
import { AVATARS, STATUS } from '../../constants';

function VoteModal({ Candidate, isActive, status, currentElectionDetails, CurrentElection, account }) {
    const [isOpen, setIsOpen] = useState(false);
    const [candidateId, setCandidateId] = useState(null);
    const [id,setId]=useState(null);
    const [val,setValue] = useState(0);
    const [approvers,setApprovers]= useState([]);

    const closeModal = e => {
        e.preventDefault();
        setIsOpen(false);
    };

    const openModal = e => {
        e.preventDefault();
        setIsOpen(true);
    };

    const handleCandidateIdChange = (e) => {
        setCandidateId(e.target.value);
    }

    const handleVoteSubmit = async (e) => {
        e.preventDefault();
        setIsOpen(false);
        try {
            window.toastProvider.addMessage("Processing your voting request.", {
                variant: "processing"
            })
            if(currentElectionDetails?.info?.algorithm == "Range"){
                await CurrentElection.vote_range(id,val).send({ from: account });
            }
            else if(currentElectionDetails?.info?.algorithm == "General"){
                await CurrentElection.vote_general(candidateId).send({ from: account });
            }
            
            window.toastProvider.addMessage("Voted", {
                secondaryMessage: "You have successfully voted! Thank you.",
                variant: "success"
            });

            setCandidateId(null);
            setIsOpen(false);
        } catch (err) {
            window.toastProvider.addMessage("Failed", {
                secondaryMessage: "Transaction failed. Try again",
                variant: "failure"
            });
        }
    }
    let detail;

    const checkVerification = async (e) => {
        e.preventDefault();

        detail = await CurrentElection.getApprovers().call()
        .then((val)=>{
            setApprovers(val);
        });
            for(let i=0;i<approvers.length;i++){
            if(account==approvers[i]) return true;
        }
        return false;
    }
    return (
        <div>
            {
                status == STATUS.ACTIVE && checkVerification
                    ?
                    <div className="voteButton" onClick={openModal}>
                        VOTE
                    </div>
                    :
                    <div className="voteButton voteButtonDisabled">
                        VOTE
                    </div>
            }
            <Modal isOpen={isOpen}>
                <Card width={"90%"} height={"80%"} p={0} style={{ maxWidth: "700px", borderRadius: "5px" }}>
                    <Button.Text
                        style={{ margin: "0px" }}
                        icononly
                        icon={"Close"}
                        color={"moon-gray"}
                        position={"absolute"}
                        top={0}
                        right={0}
                        mt={3}
                        mr={3}
                        onClick={closeModal}
                    />

                    <div style={{ margin: "10px", maxWidth: "700px", width: "90%" }}>
                        <h5>Choose candidates according to your preferences</h5>

                        <br />

                        <div>
                            <b>1st Preference</b>
                            <br /><br />

                            <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "space-between" }}>
                                {
                                    currentElectionDetails?.candidate?.map((candidate) => (
                                        <label className="voteCandidate">
                                            {currentElectionDetails?.info?.algorithm == "General"?
                                            <input type="radio" name="candidate" value={candidate?.id} onChange={handleCandidateIdChange} className="voteCandiateInput" />:
                                            <></>}
                                            
                                            <Candidate name={candidate?.name} id={candidate?.id} about={candidate?.about} voteCount={candidate?.voteCount} imageUrl={AVATARS[candidate?.id % AVATARS?.length] || '/assets/avatar.png'} />
                                            { currentElectionDetails?.info?.algorithm == "Range" ?
                                            <>
                                            <ReactStars
                                            className="voteCandiateInput"
                                            count={5}
                                            onChange={(newValue) => {
                                                setValue(newValue);
                                                setId(candidate?.id);
                                              }}
                                            size={24}
                                            color2={'#ffd700'}
                                            value={0}
                                            half={false} />
                                            <Button ml={3} type="submit" onClick={handleVoteSubmit}>Confirm</Button>
                                             </>:
                                            <p></p> 
                                            }
                                            
                                            
                                        </label>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                    { currentElectionDetails?.info?.algorithm == "General" ? <Flex
                        px={4}
                        py={3}
                        justifyContent={"flex-end"}
                    >
                        <Button.Outline onClick={closeModal}>Cancel</Button.Outline>
                        <Button ml={3} type="submit" onClick={handleVoteSubmit}>Confirm</Button>
                    </Flex>: <p></p>}
                    
                </Card>
            </Modal>
        </div>
    );
}

export default VoteModal;