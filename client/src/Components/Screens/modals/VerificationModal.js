import ReactStars from 'react-stars'
import React from 'react'
import { render } from 'react-dom'
import { useState } from 'react';
import { Flex, Modal, Button, Card, ToastMessage } from "rimble-ui";
import '../../styles/Modal.scss';
import { AVATARS, STATUS } from '../../constants';

function VerificationModal(props) {
    const [isOpen, setIsOpen] = useState(false);
    
    const closeModal = e => {
        e.preventDefault();
        setIsOpen(false);
    };

    const openModal = e => {
        e.preventDefault();
        setIsOpen(true);
    };
console.log(props)
    
    return (
        <div>
            <div className="verifyButton" onClick={openModal}>
                        Verify Candidates
                    </div>

            <Modal isOpen={isOpen}>
                <Card width={"90%"} height={"80%"} p={0} style={{ maxWidth: "700px", borderRadius: "5px" }}>
                    <Button.Text
                        style={{ margin: "0px" }}
                        icononly
                        icon={"Close"}
                        color={"moon-gray"}
                        position={"absolute"}
                        top={0}
                        right={0}
                        mt={3}
                        mr={3}
                        onClick={closeModal}
                    />
                    <h3>Verify candidates</h3>
                    <div>{props.urls}</div>
                    
                </Card>
            </Modal>
        </div>
    );
}

export default VerificationModal;