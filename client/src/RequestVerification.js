import { create } from 'ipfs-http-client'

import React from 'react'
import { render } from 'react-dom'
import { useState } from 'react';
import { Flex, Modal, Button, Card, ToastMessage } from "rimble-ui";
import './Components/styles/Modal.scss';
const client = create('https://ipfs.infura.io:5001/api/v0')


function RequestVerification(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [file, setFile] = useState(null);
    const [url, setUrl] = useState("");

    const closeModal = e => {
        e.preventDefault();
        setIsOpen(false);
    };

    const openModal = e => {
        e.preventDefault();
        setIsOpen(true);
    };
    const retrieveFile = (e) => {
        const data = e.target.files[0];
        const reader = new window.FileReader();
        reader.readAsArrayBuffer(data);
        reader.onloadend = () => {
            setFile(Buffer(reader.result));
          }
    
        e.preventDefault();  
      }

      const handleSubmit = async (e) => {
        e.preventDefault();
        try {
          const created = await client.add(file);
          const url = `https://ipfs.infura.io/ipfs/${created.path}`;
          console.log(url)
          setUrl(url);      
        } catch (error) {
          console.log(error.message);
        }
      };
    
    return (
        <div>
            <div className="verifyButton" onClick={openModal}>
                        Get Verified
                    </div>

            <Modal isOpen={isOpen}>
                <Card width={"90%"} height={"80%"} p={0} style={{ maxWidth: "700px", borderRadius: "5px" }}>
                    <Button.Text
                        style={{ margin: "0px" }}
                        icononly
                        icon={"Close"}
                        color={"moon-gray"}
                        position={"absolute"}
                        top={0}
                        right={0}
                        mt={3}
                        mr={3}
                        onClick={closeModal}
                    />
                <h3>Upload your school ID/Government Id</h3>
<div className="App">
      <form className="form" onSubmit={handleSubmit}>
        <input type="file" name="data" onChange={retrieveFile} />
        <br />
        <button type="submit" className="btn" onClick={() => props.childToParent(url)}>Upload file</button>
      </form>
    </div>
    <div className="display">
        {url}
      </div>
                </Card>
            </Modal>
        </div>
    );
}

export default RequestVerification;